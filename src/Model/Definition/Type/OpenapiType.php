<?php


namespace Frankfleige\OpenapiParser\Model\Type;

/**
 * Class OpenapiType
 * @package Frankfleige\OpenapiParser\Model\Type
 */
abstract class OpenapiType
{
    /**
     * Returns the type identifier
     * @return string
     */
    abstract public function getType(): string;

    /**
     * Returns the format of the type.
     * @return string|null
     */
    abstract public function getFormat(): ?string;
}