<?php


namespace Frankfleige\OpenapiParser\Model\Definition;

/**
 * Class Info
 * @package Frankfleige\OpenapiParser\Model\Definition
 */
class Info
{
    /**
     * @var string
     */
    private string $title;
    /**
     * @var string|null
     */
    private ?string $description = null;
    /**
     * @var string|null
     */
    private ?string $termsOfService = null;
    /**
     * @var Contact|null
     */
    private ?Contact $contact = null;
    /**
     * @var License|null
     */
    private ?License $license = null;
    /**
     * @var string
     */
    private string $version;

    /**
     * Info constructor.
     * @param string $title
     * @param string $version
     */
    public function __construct(string $title, string $version)
    {
        $this->title = $title;
        $this->version = $version;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Info
     */
    public function setTitle(string $title): Info
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return Info
     */
    public function setDescription(?string $description): Info
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTermsOfService(): ?string
    {
        return $this->termsOfService;
    }

    /**
     * @param string|null $termsOfService
     * @return Info
     */
    public function setTermsOfService(?string $termsOfService): Info
    {
        $this->termsOfService = $termsOfService;
        return $this;
    }

    /**
     * @return Contact|null
     */
    public function getContact(): ?Contact
    {
        return $this->contact;
    }

    /**
     * @param Contact|null $contact
     * @return Info
     */
    public function setContact(?Contact $contact): Info
    {
        $this->contact = $contact;
        return $this;
    }

    /**
     * @return null
     */
    public function getLicense(): ?License
    {
        return $this->license;
    }

    /**
     * @param null $license
     * @return Info
     */
    public function setLicense(?License $license): Info
    {
        $this->license = $license;
        return $this;
    }

    /**
     * @return string
     */
    public function getVersion(): string
    {
        return $this->version;
    }

    /**
     * @param string $version
     * @return Info
     */
    public function setVersion(string $version): Info
    {
        $this->version = $version;
        return $this;
    }
}