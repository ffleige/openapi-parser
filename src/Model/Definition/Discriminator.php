<?php


namespace Frankfleige\OpenapiParser\Model\Definition;

/**
 * Class Discriminator
 * @package Frankfleige\OpenapiParser\Model\Definition
 */
class Discriminator
{
    /**
     * @var string
     */
    private string $propertyName;
    /**
     * @var array
     */
    private array $mapping;

    /**
     * Discriminator constructor.
     * @param string $propertyName
     */
    public function __construct(string $propertyName)
    {
        $this->propertyName = $propertyName;
    }

    /**
     * @return string
     */
    public function getPropertyName(): string
    {
        return $this->propertyName;
    }

    /**
     * @param string $propertyName
     * @return Discriminator
     */
    public function setPropertyName(string $propertyName): Discriminator
    {
        $this->propertyName = $propertyName;
        return $this;
    }

    /**
     * @return array
     */
    public function getMapping(): array
    {
        return $this->mapping;
    }

    /**
     * @param array $mapping
     * @return Discriminator
     */
    public function setMapping(array $mapping): Discriminator
    {
        $this->mapping = $mapping;
        return $this;
    }
}