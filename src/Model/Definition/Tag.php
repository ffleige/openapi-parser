<?php


namespace Frankfleige\OpenapiParser\Model\Definition;

/**
 * Class Tag
 * @package Frankfleige\OpenapiParser\Model\Definition
 */
class Tag
{
    /**
     * @var string
     */
    private string $name;
    /**
     * @var string|null
     */
    private ?string $description = null;
    /**
     * @var ExternalDocumentation|null
     */
    private ?ExternalDocumentation $externalDocs;

    /**
     * Tag constructor.
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Tag
     */
    public function setName(string $name): Tag
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return Tag
     */
    public function setDescription(?string $description): Tag
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return ExternalDocumentation|null
     */
    public function getExternalDocs(): ?ExternalDocumentation
    {
        return $this->externalDocs;
    }

    /**
     * @param ExternalDocumentation|null $externalDocs
     * @return Tag
     */
    public function setExternalDocs(?ExternalDocumentation $externalDocs): Tag
    {
        $this->externalDocs = $externalDocs;
        return $this;
    }
}