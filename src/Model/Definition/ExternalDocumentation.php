<?php


namespace Frankfleige\OpenapiParser\Model\Definition;

/**
 * Class ExternalDocumentation
 * @package Frankfleige\OpenapiParser\Model\Definition
 */
class ExternalDocumentation
{
    /**
     * @var string|null
     */
    private ?string $description = null;
    /**
     * @var string
     */
    private string $url;

    /**
     * ExternalDocumentation constructor.
     * @param string $url
     */
    public function __construct(string $url)
    {
        $this->url = $url;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return ExternalDocumentation
     */
    public function setDescription(?string $description): ExternalDocumentation
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return ExternalDocumentation
     */
    public function setUrl(string $url): ExternalDocumentation
    {
        $this->url = $url;
        return $this;
    }
}