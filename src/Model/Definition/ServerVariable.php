<?php


namespace Frankfleige\OpenapiParser\Model\Definition;

/**
 * Class ServerVariable
 * @package Frankfleige\OpenapiParser\Model\Definition
 */
class ServerVariable
{
    /**
     * @var string[]
     */
    private array $enum = [];
    /**
     * @var string
     */
    private string $default;
    /**
     * @var string|null
     */
    private ?string $description;

    /**
     * ServerVariable constructor.
     * @param string $default
     */
    public function __construct(string $default)
    {
        $this->default = $default;
    }

    /**
     * @return string[]
     */
    public function getEnum(): array
    {
        return $this->enum;
    }

    /**
     * @param string[] $enum
     * @return ServerVariable
     */
    public function setEnum(array $enum): ServerVariable
    {
        $this->enum = $enum;
        return $this;
    }

    /**
     * @return string
     */
    public function getDefault(): string
    {
        return $this->default;
    }

    /**
     * @param string $default
     * @return ServerVariable
     */
    public function setDefault(string $default): ServerVariable
    {
        $this->default = $default;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return ServerVariable
     */
    public function setDescription(?string $description): ServerVariable
    {
        $this->description = $description;
        return $this;
    }
}