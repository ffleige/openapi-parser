<?php


namespace Frankfleige\OpenapiParser\Model\Definition;

/**
 * Class Security
 * @package Frankfleige\OpenapiParser\Model\Definition
 */
class Security
{
    /**
     * @var string
     */
    private string $name;
    /**
     * @var array
     */
    private array $values = [];

    /**
     * Security constructor.
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Security
     */
    public function setName(string $name): Security
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return array
     */
    public function getValues(): array
    {
        return $this->values;
    }

    /**
     * @param array $values
     * @return Security
     */
    public function setValues(array $values): Security
    {
        $this->values = $values;
        return $this;
    }
}