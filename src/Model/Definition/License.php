<?php


namespace Frankfleige\OpenapiParser\Model\Definition;

/**
 * Class License
 * @package Frankfleige\OpenapiParser\Model\Definition
 */
class License
{
    /**
     * @var string
     */
    private string $name;
    /**
     * @var string|null
     */
    private ?string $url = null;

    /**
     * License constructor.
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return License
     */
    public function setName(string $name): License
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @param string|null $url
     * @return License
     */
    public function setUrl(?string $url): License
    {
        $this->url = $url;
        return $this;
    }


}