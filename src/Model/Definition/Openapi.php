<?php


namespace Frankfleige\OpenapiParser\Model\Definition;

/**
 * Class Openapi
 * @package Frankfleige\OpenapiParser\Model\Definition
 */
class Openapi
{
    /**
     * @var string
     */
    private string $version;
    /**
     * @var Info
     */
    private Info $info;
    /**
     * @var Server[]
     */
    private array $servers = [];

}