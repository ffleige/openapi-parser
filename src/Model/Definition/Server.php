<?php


namespace Frankfleige\OpenapiParser\Model\Definition;

/**
 * Class Server
 * @package Frankfleige\OpenapiParser\Model\Definition
 */
class Server
{
    /**
     * @var string
     */
    private string $url;
    /**
     * @var string|null
     */
    private ?string $description = null;
    /**
     * @var string[]|ServerVariable[]
     */
    private array $variables = [];

    /**
     * Server constructor.
     * @param string $url
     */
    public function __construct(string $url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return Server
     */
    public function setUrl(string $url): Server
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return Server
     */
    public function setDescription(?string $description): Server
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return ServerVariable[]|string[]
     */
    public function getVariables(): array
    {
        return $this->variables;
    }

    /**
     * @param ServerVariable[]|string[] $variables
     * @return Server
     */
    public function setVariables(array $variables): Server
    {
        $this->variables = $variables;
        return $this;
    }
}