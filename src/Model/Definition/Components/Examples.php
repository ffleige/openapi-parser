<?php


namespace Frankfleige\OpenapiParser\Model\Definition\Components;

/**
 * Class Examples
 * @package Frankfleige\OpenapiParser\Model\Definition\Components
 */
class Examples
{
    /**
     * @var string|null
     */
    private ?string $summary = null;
    /**
     * @var string|null
     */
    private ?string $description = null;
    /**
     * @var mixed|null
     */
    private $value = null;
    /**
     * @var string|null
     */
    private ?string $externalValue = null;

    /**
     * @return string|null
     */
    public function getSummary(): ?string
    {
        return $this->summary;
    }

    /**
     * @param string|null $summary
     * @return Examples
     */
    public function setSummary(?string $summary): Examples
    {
        $this->summary = $summary;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return Examples
     */
    public function setDescription(?string $description): Examples
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed|null
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed|null $value
     * @return Examples
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getExternalValue(): ?string
    {
        return $this->externalValue;
    }

    /**
     * @param string|null $externalValue
     * @return Examples
     */
    public function setExternalValue(?string $externalValue): Examples
    {
        $this->externalValue = $externalValue;
        return $this;
    }
}