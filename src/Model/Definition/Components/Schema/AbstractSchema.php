<?php


namespace Frankfleige\OpenapiParser\Model\Definition\Components\Schema;

use Frankfleige\OpenapiParser\Model\Definition\Discriminator;
use Frankfleige\OpenapiParser\Model\Definition\ExternalDocumentation;

/**
 * Class AbstractSchema
 * @package Frankfleige\OpenapiParser\Model\Definition\Components\Schema
 */
abstract class AbstractSchema
{
    /**
     * @var string|null
     */
    private ?string $title = null;
    /**
     * @var string|null
     */
    private ?string $description = null;
    /**
     * @var mixed|null
     */
    private $default = null;
    /**
     * @var bool
     */
    private bool $nullable = false;
    /**
     * @var bool
     */
    private bool $readOnly = false;
    /**
     * @var bool
     */
    private bool $writeOnly = false;
    /**
     * @var ExternalDocumentation|null
     */
    private ?ExternalDocumentation $externalDocs = null;
    /**
     * @var mixed|null
     */
    private $example = null;
    /**
     * @var bool
     */
    private bool $deprecated = false;
    /**
     * @var Discriminator|null
     */
    private ?Discriminator $discriminator = null;
    /**
     * @var array
     */
    private array $enum = [];
    /**
     * @var AbstractSchema[]
     */
    private array $allOf = [];
    /**
     * @var AbstractSchema[]
     */
    private array $anyOf = [];
    /**
     * @var AbstractSchema[]
     */
    private array $oneOf = [];

    /**
     * @return AbstractSchema[]
     */
    public function getAllOf(): array
    {
        return $this->allOf;
    }

    /**
     * @param AbstractSchema[] $allOf
     * @return AbstractSchema
     */
    public function setAllOf(array $allOf): AbstractSchema
    {
        $this->allOf = $allOf;
        return $this;
    }

    /**
     * @return AbstractSchema[]
     */
    public function getAnyOf(): array
    {
        return $this->anyOf;
    }

    /**
     * @param AbstractSchema[] $anyOf
     * @return AbstractSchema
     */
    public function setAnyOf(array $anyOf): AbstractSchema
    {
        $this->anyOf = $anyOf;
        return $this;
    }

    /**
     * @return AbstractSchema[]
     */
    public function getOneOf(): array
    {
        return $this->oneOf;
    }

    /**
     * @param AbstractSchema[] $oneOf
     * @return AbstractSchema
     */
    public function setOneOf(array $oneOf): AbstractSchema
    {
        $this->oneOf = $oneOf;
        return $this;
    }

    /**
     * @return array
     */
    public function getEnum(): array
    {
        return $this->enum;
    }

    /**
     * @param array $enum
     * @return AbstractSchema
     */
    public function setEnum(array $enum): AbstractSchema
    {
        $this->enum = $enum;
        return $this;
    }

    /**
     * @return Discriminator|null
     */
    public function getDiscriminator(): ?Discriminator
    {
        return $this->discriminator;
    }

    /**
     * @param Discriminator|null $discriminator
     * @return AbstractSchema
     */
    public function setDiscriminator(?Discriminator $discriminator): AbstractSchema
    {
        $this->discriminator = $discriminator;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     * @return AbstractSchema
     */
    public function setTitle(?string $title): AbstractSchema
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    abstract public function getType(): string;

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return AbstractSchema
     */
    public function setDescription(?string $description): AbstractSchema
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed|null
     */
    public function getDefault()
    {
        return $this->default;
    }

    /**
     * @param mixed|null $default
     * @return AbstractSchema
     */
    public function setDefault($default)
    {
        $this->default = $default;
        return $this;
    }

    /**
     * @return bool
     */
    public function isNullable(): bool
    {
        return $this->nullable;
    }

    /**
     * @param bool $nullable
     * @return AbstractSchema
     */
    public function setNullable(bool $nullable): AbstractSchema
    {
        $this->nullable = $nullable;
        return $this;
    }

    /**
     * @return bool
     */
    public function isReadOnly(): bool
    {
        return $this->readOnly;
    }

    /**
     * @param bool $readOnly
     * @return AbstractSchema
     */
    public function setReadOnly(bool $readOnly): AbstractSchema
    {
        $this->readOnly = $readOnly;
        return $this;
    }

    /**
     * @return bool
     */
    public function isWriteOnly(): bool
    {
        return $this->writeOnly;
    }

    /**
     * @param bool $writeOnly
     * @return AbstractSchema
     */
    public function setWriteOnly(bool $writeOnly): AbstractSchema
    {
        $this->writeOnly = $writeOnly;
        return $this;
    }

    /**
     * @return ExternalDocumentation|null
     */
    public function getExternalDocs(): ?ExternalDocumentation
    {
        return $this->externalDocs;
    }

    /**
     * @param ExternalDocumentation|null $externalDocs
     * @return AbstractSchema
     */
    public function setExternalDocs(?ExternalDocumentation $externalDocs): AbstractSchema
    {
        $this->externalDocs = $externalDocs;
        return $this;
    }

    /**
     * @return mixed|null
     */
    public function getExample()
    {
        return $this->example;
    }

    /**
     * @param mixed|null $example
     * @return AbstractSchema
     */
    public function setExample($example)
    {
        $this->example = $example;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDeprecated(): bool
    {
        return $this->deprecated;
    }

    /**
     * @param bool $deprecated
     * @return AbstractSchema
     */
    public function setDeprecated(bool $deprecated): AbstractSchema
    {
        $this->deprecated = $deprecated;
        return $this;
    }
}