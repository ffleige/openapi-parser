<?php


namespace Frankfleige\OpenapiParser\Model\Definition\Components\Schema;

/**
 * Class ObjectSchema
 * @package Frankfleige\OpenapiParser\Model\Definition\Components\Schema
 */
class ObjectSchema extends AbstractSchema
{
    /**
     * @var AbstractSchema[]
     */
    private array $properties = [];
    /**
     * @var AbstractSchema[]
     */
    private array $additionalProperties = [];
    /**
     * @var string[]
     */
    private array $required = [];
    /**
     * @var int|null
     */
    private ?int $minProperties = null;
    /**
     * @var int|null
     */
    private ?int $maxProperties = null;

    /**
     * @return int|null
     */
    public function getMinProperties(): ?int
    {
        return $this->minProperties;
    }

    /**
     * @param int|null $minProperties
     * @return ObjectSchema
     */
    public function setMinProperties(?int $minProperties): ObjectSchema
    {
        $this->minProperties = $minProperties;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getMaxProperties(): ?int
    {
        return $this->maxProperties;
    }

    /**
     * @param int|null $maxProperties
     * @return ObjectSchema
     */
    public function setMaxProperties(?int $maxProperties): ObjectSchema
    {
        $this->maxProperties = $maxProperties;
        return $this;
    }

    /**
     * @return string[]
     */
    public function getRequired(): array
    {
        return $this->required;
    }

    /**
     * @param string[] $required
     * @return ObjectSchema
     */
    public function setRequired(array $required): ObjectSchema
    {
        $this->required = $required;
        return $this;
    }

    /**
     * @return AbstractSchema[]
     */
    public function getProperties(): array
    {
        return $this->properties;
    }

    /**
     * @param AbstractSchema[] $properties
     * @return ObjectSchema
     */
    public function setProperties(array $properties): ObjectSchema
    {
        $this->properties = $properties;
        return $this;
    }

    /**
     * @return AbstractSchema[]
     */
    public function getAdditionalProperties(): array
    {
        return $this->additionalProperties;
    }

    /**
     * @param AbstractSchema[] $additionalProperties
     * @return ObjectSchema
     */
    public function setAdditionalProperties(array $additionalProperties): ObjectSchema
    {
        $this->additionalProperties = $additionalProperties;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'object';
    }
}