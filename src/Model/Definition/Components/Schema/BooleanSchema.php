<?php


namespace Frankfleige\OpenapiParser\Model\Definition\Components\Schema;

/**
 * Class BooleanSchema
 * @package Frankfleige\OpenapiParser\Model\Definition\Components\Schema
 */
class BooleanSchema extends AbstractSchema
{
    /**
     * @return string
     */
    public function getType(): string
    {
        return 'boolean';
    }
}