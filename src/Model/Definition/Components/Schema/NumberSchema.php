<?php


namespace Frankfleige\OpenapiParser\Model\Definition\Components\Schema;

/**
 * Class NumberSchema
 * @package Frankfleige\OpenapiParser\Model\Definition\Components\Schema
 */
class NumberSchema extends AbstractSchema
{
    /**
     * @var float|null
     */
    private ?float $maximum = null;
    /**
     * @var float|null
     */
    private ?float $exclusiveMaximum = null;
    /**
     * @var float|null
     */
    private ?float $minimum = null;
    /**
     * @var float|null
     */
    private ?float $exclusiveMinimum = null;
    /**
     * @var float|null
     */
    private ?float $multipleOf = null;

    /**
     * @return float|null
     */
    public function getMaximum(): ?float
    {
        return $this->maximum;
    }

    /**
     * @param float|null $maximum
     * @return NumberSchema
     */
    public function setMaximum(?float $maximum): NumberSchema
    {
        $this->maximum = $maximum;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getExclusiveMaximum(): ?float
    {
        return $this->exclusiveMaximum;
    }

    /**
     * @param float|null $exclusiveMaximum
     * @return NumberSchema
     */
    public function setExclusiveMaximum(?float $exclusiveMaximum): NumberSchema
    {
        $this->exclusiveMaximum = $exclusiveMaximum;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getMinimum(): ?float
    {
        return $this->minimum;
    }

    /**
     * @param float|null $minimum
     * @return NumberSchema
     */
    public function setMinimum(?float $minimum): NumberSchema
    {
        $this->minimum = $minimum;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getExclusiveMinimum(): ?float
    {
        return $this->exclusiveMinimum;
    }

    /**
     * @param float|null $exclusiveMinimum
     * @return NumberSchema
     */
    public function setExclusiveMinimum(?float $exclusiveMinimum): NumberSchema
    {
        $this->exclusiveMinimum = $exclusiveMinimum;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getMultipleOf(): ?float
    {
        return $this->multipleOf;
    }

    /**
     * @param float|null $multipleOf
     * @return NumberSchema
     */
    public function setMultipleOf(?float $multipleOf): NumberSchema
    {
        $this->multipleOf = $multipleOf;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'number';
    }
}