<?php


namespace Frankfleige\OpenapiParser\Model\Definition\Components\Schema;

/**
 * Class StringSchema
 * @package Frankfleige\OpenapiParser\Model\Definition\Components\Schema
 */
class StringSchema extends AbstractSchema
{
    /**
     * @var int|null
     */
    private ?int $maxLength = null;
    /**
     * @var int|null
     */
    private ?int $minLength = null;
    /**
     * @var string|null
     */
    private ?string $pattern = null;
    /**
     * @return int|null
     */
    public function getMaxLength(): ?int
    {
        return $this->maxLength;
    }

    /**
     * @param int|null $maxLength
     * @return StringSchema
     */
    public function setMaxLength(?int $maxLength): StringSchema
    {
        $this->maxLength = $maxLength;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getMinLength(): ?int
    {
        return $this->minLength;
    }

    /**
     * @param int|null $minLength
     * @return StringSchema
     */
    public function setMinLength(?int $minLength): StringSchema
    {
        $this->minLength = $minLength;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPattern(): ?string
    {
        return $this->pattern;
    }

    /**
     * @param string|null $pattern
     * @return StringSchema
     */
    public function setPattern(?string $pattern): StringSchema
    {
        $this->pattern = $pattern;
        return $this;
    }
    /**
     * @return string
     */
    public function getType(): string
    {
        return 'string';
    }
}