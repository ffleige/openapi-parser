<?php


namespace Frankfleige\OpenapiParser\Model\Definition\Components\Schema;

/**
 * Class ArraySchema
 * @package Frankfleige\OpenapiParser\Model\Definition\Components\Schema
 */
class ArraySchema extends AbstractSchema
{
    /**
     * @var int|null
     */
    private ?int $minItems = null;
    /**
     * @var int|null
     */
    private ?int $maxItems = null;
    /**
     * @var AbstractSchema[]
     */
    private array $items = [];
    /**
     * @var bool
     */
    private bool $uniqueItems = false;

    /**
     * @return bool
     */
    public function isUniqueItems(): bool
    {
        return $this->uniqueItems;
    }

    /**
     * @param bool $uniqueItems
     * @return ArraySchema
     */
    public function setUniqueItems(bool $uniqueItems): ArraySchema
    {
        $this->uniqueItems = $uniqueItems;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getMinItems(): ?int
    {
        return $this->minItems;
    }

    /**
     * @param int|null $minItems
     * @return ArraySchema
     */
    public function setMinItems(?int $minItems): ArraySchema
    {
        $this->minItems = $minItems;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getMaxItems(): ?int
    {
        return $this->maxItems;
    }

    /**
     * @param int|null $maxItems
     * @return ArraySchema
     */
    public function setMaxItems(?int $maxItems): ArraySchema
    {
        $this->maxItems = $maxItems;
        return $this;
    }

    /**
     * @return AbstractSchema[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param AbstractSchema[] $items
     * @return ArraySchema
     */
    public function setItems(array $items): ArraySchema
    {
        $this->items = $items;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'array';
    }
}