<?php


namespace Frankfleige\OpenapiParser\Model\Definition\Components\Schema;

/**
 * Class IntegerSchema
 * @package Frankfleige\OpenapiParser\Model\Definition\Components\Schema
 */
class IntegerSchema extends AbstractSchema
{
    /**
     * @var int|null
     */
    private ?int $maximum = null;
    /**
     * @var int|null
     */
    private ?int $exclusiveMaximum = null;
    /**
     * @var int|null
     */
    private ?int $minimum = null;
    /**
     * @var int|null
     */
    private ?int $exclusiveMinimum = null;
    /**
     * @var int|null
     */
    private ?int $multipleOf = null;

    /**
     * @return int|null
     */
    public function getMaximum(): ?int
    {
        return $this->maximum;
    }

    /**
     * @param int|null $maximum
     * @return IntegerSchema
     */
    public function setMaximum(?int $maximum): IntegerSchema
    {
        $this->maximum = $maximum;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getExclusiveMaximum(): ?int
    {
        return $this->exclusiveMaximum;
    }

    /**
     * @param int|null $exclusiveMaximum
     * @return IntegerSchema
     */
    public function setExclusiveMaximum(?int $exclusiveMaximum): IntegerSchema
    {
        $this->exclusiveMaximum = $exclusiveMaximum;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getMinimum(): ?int
    {
        return $this->minimum;
    }

    /**
     * @param int|null $minimum
     * @return IntegerSchema
     */
    public function setMinimum(?int $minimum): IntegerSchema
    {
        $this->minimum = $minimum;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getExclusiveMinimum(): ?int
    {
        return $this->exclusiveMinimum;
    }

    /**
     * @param int|null $exclusiveMinimum
     * @return IntegerSchema
     */
    public function setExclusiveMinimum(?int $exclusiveMinimum): IntegerSchema
    {
        $this->exclusiveMinimum = $exclusiveMinimum;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getMultipleOf(): ?int
    {
        return $this->multipleOf;
    }

    /**
     * @param int|null $multipleOf
     * @return IntegerSchema
     */
    public function setMultipleOf(?int $multipleOf): IntegerSchema
    {
        $this->multipleOf = $multipleOf;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'integer';
    }
}